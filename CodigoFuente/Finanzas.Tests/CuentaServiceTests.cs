﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finanzas.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.App.Services.Tests
{
    [TestClass()]
    public class CuentaServiceTests
    {
        [TestMethod()]
        public void AgregarCuentaTest()
        {
            CuentaService cs = new CuentaService();
            Models.Binding.Cuenta c = new Models.Binding.Cuenta()
            {
                Descripcion = "Es una prueba",
                FechaInicio = DateTime.Now.AddDays(-30),
                Monto = 12000.00,
                TEA = 0.3,
            };
            cs.AgregarCuenta(1, c);
            //Assert.Fail();
        }

        [TestMethod()]
        public void AgregarMovimientoFinalTest()
        {
            CuentaService cs = new CuentaService();
            cs.AgregarMovimientoFinal(1, new Models.Binding.Movimiento()
            {
                CuentaId = 2,
                Descripcion = "prueba 3",
                Fecha = DateTime.Now.AddDays(-5),
                IntDescuento = 0,
                MonDescuento = 0,
                Monto = 3000,
            });
        }
    }
}