﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Identity.Models
{
    /// <summary>
    /// A basic implementation for an application database context compatible with ASP.NET Identity 2 using
    /// <see cref="long"/> as the key-column-type for all entities.
    /// </summary>
    /// <remarks>
    /// This type depends on some other types out of this assembly.
    /// </remarks>
    public class ApplicationDbContext : IdentityDbContext<CustomUser, CustomRole, long,
        CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        #region constructors and destructors

        //definir la conexion (ConnectionString)
        public ApplicationDbContext() : base("IdentityConnection")
        {
        }

        #endregion

        #region methods

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Map Entities to their tables.
            modelBuilder.Entity<CustomUser>().ToTable("Usuarios");
            modelBuilder.Entity<CustomRole>().ToTable("Roles");
            modelBuilder.Entity<CustomUserClaim>().ToTable("UsuarioClaims");
            modelBuilder.Entity<CustomUserLogin>().ToTable("UsuarioLogins");
            modelBuilder.Entity<CustomUserRole>().ToTable("UsuarioRoles");
            // Set AutoIncrement-Properties
            modelBuilder.Entity<CustomUser>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<CustomUserClaim>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<CustomRole>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            // Override some column mappings that do not match our default
            modelBuilder.Entity<CustomUser>().Property(r => r.UserName).HasColumnName("Usuario");
            //modelBuilder.Entity<CustomUser>().Property(r => r.PasswordHash).HasColumnName("Password");
        }

        #endregion
    }
}
