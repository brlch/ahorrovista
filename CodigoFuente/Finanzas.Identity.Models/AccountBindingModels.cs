﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Finanzas.Identity.Models
{
    // Models used as parameters to AccountController actions.
    public class Binding
    {
        public class AddExternalLogin
        {
            [Required]
            [Display(Name = "External access token")]
            public string ExternalAccessToken { get; set; }
        }

        public class ChangePassword
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public class Register
        {
            [Required]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "La {0} debe tener al menos {2} caracteres.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Contraseña")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "La contraseña no coincide.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Nombres", Description = "Nombres del Usuario")]
            [StringLength(100, ErrorMessage = "Los {0} debe tener al menos {2} caracteres.", MinimumLength = 1)]
            public string Nombres { get; set; }

            [Required]
            [Display(Name = "Apellidos", Description = "Apellidos del Usuario")]
            [StringLength(100, ErrorMessage = "Los {0} deben tener al menos {2} caracteres.", MinimumLength = 1)]
            public string Apellidos { get; set; }
        }

        public class Login
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Contraseña")]
            public string Password { get; set; }

            [Display(Name = "Recordarme")]
            public bool RememberMe { get; set; }
        }

        public class RegisterExternal
        {
            [Required]
            [Display(Name = "Email")]
            public string Email { get; set; }
        }

        public class RemoveLogin
        {
            [Required]
            [Display(Name = "Login provider")]
            public string LoginProvider { get; set; }

            [Required]
            [Display(Name = "Provider key")]
            public string ProviderKey { get; set; }
        }

        public class SetPassword
        {
            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }
    }
}
