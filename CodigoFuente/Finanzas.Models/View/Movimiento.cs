﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Models.View
{
    public class Movimiento
    {
        public DateTime Fecha { get; set; }
        public double Monto { get; set; }
        public double? IntDescuento { get; set; }
        public double? MonDescuento { get; set; }
        public double? TEA { get; set; }
        public string Descripcion { get; set; }
    }
}
