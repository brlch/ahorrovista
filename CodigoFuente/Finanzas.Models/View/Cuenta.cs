﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Models.View
{
    public class Cuenta
    {
        public long id { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public double Monto { get; set; }
        public double MontoActual { get; set; }
        public string Descripcion { get; set; }
        public List<Movimiento> Movimientos { get; set; }
        

    }
}
