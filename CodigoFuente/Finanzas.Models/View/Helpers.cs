﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finanzas.Helpers;

namespace Finanzas.Models.View
{
    public static class Converters
    {
        public static Movimiento toMovimiento(this Movimientos obj)
        {
            Movimiento m = new Movimiento()
            {
                TEA = obj.TEA.toDouble(),
                Descripcion = obj.Descripcion,
                Fecha = obj.Fecha,
                IntDescuento = obj.InteresDesc.toDouble(),
                MonDescuento = obj.MontoDesc.toDouble(),
                Monto = obj.Monto.toDouble()
            };
            return m;
        }
        public static Cuenta toCuenta(this Models.Cuentas obj, List<Movimientos> movimientos = null)
        {
            Cuenta c = new Cuenta()
            {
                Descripcion = obj.Descripcion,
                FechaInicio = obj.FechaInicio,
                FechaFin = obj.FechaFin,
                Monto = obj.Monto.toDouble(),
                id = obj.Id
            };
            if (movimientos != null)
            {
                List<Movimiento> lista = new List<Movimiento>();
                foreach (var item in movimientos)
                {
                    lista.Add(item.toMovimiento());
                }
                c.Movimientos = lista;
            }

            return c;
        }
        

    }
}
