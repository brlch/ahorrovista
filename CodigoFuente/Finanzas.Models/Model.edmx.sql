
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/17/2015 18:15:29
-- Generated from EDMX file: D:\Users\Jean Pierre\Documents\Visual Studio 2015\Projects\Finanzas\Finanzas.Models\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Finanzas];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Cuentas_Usuarios]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cuentas] DROP CONSTRAINT [FK_Cuentas_Usuarios];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_UsuarioClaims_dbo_Usuarios_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsuarioClaims] DROP CONSTRAINT [FK_dbo_UsuarioClaims_dbo_Usuarios_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_UsuarioLogins_dbo_Usuarios_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsuarioLogins] DROP CONSTRAINT [FK_dbo_UsuarioLogins_dbo_Usuarios_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_UsuarioRoles_dbo_Roles_RoleId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsuarioRoles] DROP CONSTRAINT [FK_dbo_UsuarioRoles_dbo_Roles_RoleId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_UsuarioRoles_dbo_Usuarios_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsuarioRoles] DROP CONSTRAINT [FK_dbo_UsuarioRoles_dbo_Usuarios_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_Movimientos_Cuentas]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Movimientos] DROP CONSTRAINT [FK_Movimientos_Cuentas];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Cuentas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cuentas];
GO
IF OBJECT_ID(N'[dbo].[Movimientos]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Movimientos];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[UsuarioClaims]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsuarioClaims];
GO
IF OBJECT_ID(N'[dbo].[UsuarioLogins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsuarioLogins];
GO
IF OBJECT_ID(N'[dbo].[UsuarioRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsuarioRoles];
GO
IF OBJECT_ID(N'[dbo].[Usuarios]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuarios];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Cuentas'
CREATE TABLE [dbo].[Cuentas] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaFin] datetime  NULL,
    [Monto] decimal(18,2)  NOT NULL,
    [Simulacion] bit  NOT NULL,
    [Estado] bit  NOT NULL,
    [UsuarioId] bigint  NOT NULL,
    [Descripcion] varchar(max)  NULL
);
GO

-- Creating table 'Movimientos'
CREATE TABLE [dbo].[Movimientos] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Monto] decimal(18,2)  NOT NULL,
    [Fecha] datetime  NOT NULL,
    [InteresDesc] decimal(18,6)  NOT NULL,
    [MontoDesc] decimal(18,2)  NOT NULL,
    [Descripcion] varchar(max)  NULL,
    [TEA] decimal(18,6)  NOT NULL,
    [CuentaId] bigint  NOT NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'UsuarioClaims'
CREATE TABLE [dbo].[UsuarioClaims] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [UserId] bigint  NOT NULL,
    [ClaimType] nvarchar(max)  NULL,
    [ClaimValue] nvarchar(max)  NULL
);
GO

-- Creating table 'UsuarioLogins'
CREATE TABLE [dbo].[UsuarioLogins] (
    [LoginProvider] nvarchar(128)  NOT NULL,
    [ProviderKey] nvarchar(128)  NOT NULL,
    [UserId] bigint  NOT NULL
);
GO

-- Creating table 'Usuarios'
CREATE TABLE [dbo].[Usuarios] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [Usuario] nvarchar(256)  NOT NULL,
    [Nombres] nvarchar(256)  NOT NULL,
    [Apellidos] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'UsuarioRoles'
CREATE TABLE [dbo].[UsuarioRoles] (
    [Roles_Id] bigint  NOT NULL,
    [Usuarios_Id] bigint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Cuentas'
ALTER TABLE [dbo].[Cuentas]
ADD CONSTRAINT [PK_Cuentas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Movimientos'
ALTER TABLE [dbo].[Movimientos]
ADD CONSTRAINT [PK_Movimientos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UsuarioClaims'
ALTER TABLE [dbo].[UsuarioClaims]
ADD CONSTRAINT [PK_UsuarioClaims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LoginProvider], [ProviderKey], [UserId] in table 'UsuarioLogins'
ALTER TABLE [dbo].[UsuarioLogins]
ADD CONSTRAINT [PK_UsuarioLogins]
    PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey], [UserId] ASC);
GO

-- Creating primary key on [Id] in table 'Usuarios'
ALTER TABLE [dbo].[Usuarios]
ADD CONSTRAINT [PK_Usuarios]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Roles_Id], [Usuarios_Id] in table 'UsuarioRoles'
ALTER TABLE [dbo].[UsuarioRoles]
ADD CONSTRAINT [PK_UsuarioRoles]
    PRIMARY KEY CLUSTERED ([Roles_Id], [Usuarios_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UsuarioId] in table 'Cuentas'
ALTER TABLE [dbo].[Cuentas]
ADD CONSTRAINT [FK_Cuentas_Usuarios]
    FOREIGN KEY ([UsuarioId])
    REFERENCES [dbo].[Usuarios]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Cuentas_Usuarios'
CREATE INDEX [IX_FK_Cuentas_Usuarios]
ON [dbo].[Cuentas]
    ([UsuarioId]);
GO

-- Creating foreign key on [CuentaId] in table 'Movimientos'
ALTER TABLE [dbo].[Movimientos]
ADD CONSTRAINT [FK_Movimientos_Cuentas]
    FOREIGN KEY ([CuentaId])
    REFERENCES [dbo].[Cuentas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Movimientos_Cuentas'
CREATE INDEX [IX_FK_Movimientos_Cuentas]
ON [dbo].[Movimientos]
    ([CuentaId]);
GO

-- Creating foreign key on [UserId] in table 'UsuarioClaims'
ALTER TABLE [dbo].[UsuarioClaims]
ADD CONSTRAINT [FK_dbo_UsuarioClaims_dbo_Usuarios_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Usuarios]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_UsuarioClaims_dbo_Usuarios_UserId'
CREATE INDEX [IX_FK_dbo_UsuarioClaims_dbo_Usuarios_UserId]
ON [dbo].[UsuarioClaims]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'UsuarioLogins'
ALTER TABLE [dbo].[UsuarioLogins]
ADD CONSTRAINT [FK_dbo_UsuarioLogins_dbo_Usuarios_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Usuarios]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_UsuarioLogins_dbo_Usuarios_UserId'
CREATE INDEX [IX_FK_dbo_UsuarioLogins_dbo_Usuarios_UserId]
ON [dbo].[UsuarioLogins]
    ([UserId]);
GO

-- Creating foreign key on [Roles_Id] in table 'UsuarioRoles'
ALTER TABLE [dbo].[UsuarioRoles]
ADD CONSTRAINT [FK_UsuarioRoles_Roles]
    FOREIGN KEY ([Roles_Id])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Usuarios_Id] in table 'UsuarioRoles'
ALTER TABLE [dbo].[UsuarioRoles]
ADD CONSTRAINT [FK_UsuarioRoles_Usuarios]
    FOREIGN KEY ([Usuarios_Id])
    REFERENCES [dbo].[Usuarios]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioRoles_Usuarios'
CREATE INDEX [IX_FK_UsuarioRoles_Usuarios]
ON [dbo].[UsuarioRoles]
    ([Usuarios_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------