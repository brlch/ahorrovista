﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Models.Binding
{
   public class Cuenta
    {
        [Required(ErrorMessage = "Ingrese una fecha válida")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Inicio")]
        public DateTime FechaInicio { get; set; }

        [Required(ErrorMessage = "Ingrese un Monto válida")]
        [Display(Name = "Monto Inicial")]
        [Range(0.0000001, Double.MaxValue, ErrorMessage = "El número ingresado debe ser positivo")]
        public double Monto { get; set; }

        [Required(ErrorMessage = "Ingrese una TEA válida")]
        [Display(Name = "TEA - Tasa Efectiva Anual")]
        [Range(0.0000001, Double.MaxValue, ErrorMessage = "El número ingresado debe ser positivo")]
        public double TEA { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Descripción Breve")]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string Descripcion { get; set; }
    }
}
