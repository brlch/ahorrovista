﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Models.Binding
{
    public class ValorFecha
    {
        [Required]
        [Display(Name = "Id de cuenta")]
        public long CuentaId { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Valor")]
        public DateTime Fecha { get; set; }

    }
}
