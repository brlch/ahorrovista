﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
//using Newtonsoft.Json;

namespace Finanzas.Models.Binding
{
    public class Movimiento
    {
        [Required]
        public long CuentaId { get; set; }

        [Required(ErrorMessage = "Ingrese una fecha válida")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de la Transacción")]
        public DateTime Fecha { get; set; }

        [Required(ErrorMessage = "Ingrese un Monto válida")]
        [DataType(DataType.Currency,ErrorMessage ="Ingrese un Monto correcto", ErrorMessageResourceName ="Monto")]
        [Range(0.0000001, Double.MaxValue, ErrorMessage = "El número ingresado debe ser positivo")]
        [Display(Name = "Monto de la Transacción")]
        public double Monto { get; set; }

        [Required]
        public bool Retiro { get; set; }

        [Display(Name = "Porcentaje Descontado")]
        public double IntDescuento { get; set; }

        [Display(Name = "MontoDescontado")]
        [Range(0.0000001, Double.MaxValue, ErrorMessage = "El número ingresado debe ser positivo")]
        public double MonDescuento { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Descripción Breve")]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string Descripcion { get; set; }
    }
}
