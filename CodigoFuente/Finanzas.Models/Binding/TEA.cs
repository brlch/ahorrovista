﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Models.Binding
{
    public class TEA
    {
        [Required]
        [Display(Name = "Id de cuenta")]
        public long CuentaId { get; set; }

        [Required(ErrorMessage = "Ingrese una TEA válida")]
        [Display(Name = "Tasa Efectiva Anual")]
        [Range(0.0000001, Double.MaxValue, ErrorMessage = "El número ingresado debe ser positiva")]
        public double tea { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Descripción Breve")]
        [StringLength(256, ErrorMessage = "La {0} debe ser de al menos {2} caracteres.", MinimumLength = 0)]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "Ingrese una fecha válida")]
        [DataType(DataType.Date, ErrorMessage = "Ingrese una fecha válida")]
        [Display(Name = "Fecha")]
        public DateTime Fecha { get; set; }
    }
}
