﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finanzas.Helpers
{
    public static class Helpers
    {
        #region Converters
        public static decimal toDecimal(this double obj)
        {
            return (decimal)obj;
        }
        public static double toDouble(this decimal obj)
        {
            return (double)obj;
        }
        #endregion
    }
    public class JSONResponse
    {
        public string status { get; set; }
        public List<string> errors { get; set; }
        public string information { get; set; }
    }
}
