﻿using Finanzas.App.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finanzas.App.Controllers
{
    public class SimulacionController : Controller
    {
        // GET: Ahorro
        [HttpGet]
        public ActionResult Index(long? id)
        {
            CuentaService cs = new CuentaService();
            Models.View.Cuenta model = null;
            if (id.HasValue)
                model = cs.GetCuenta(User.Identity.GetUserId<long>(), id.Value, esSimulacion: true);
            if (model == null)
                return View(cs.GetCuentas(User.Identity.GetUserId<long>(), true));
            else
                return View("IndexSimulacion", model);
        }


        [HttpGet]
        public ActionResult CrearSimulacion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CrearSimulacion(Models.Binding.Cuenta cuenta)
        {
            if (!ModelState.IsValid)
                return View(cuenta);
            CuentaService cs = new CuentaService();
            long Id = cs.AgregarCuenta(User.Identity.GetUserId<long>(), cuenta, true);
            return RedirectToAction("Index", new { id = Id });
            //redirect to Cuenta-> id cuenta
        }
        [HttpGet]
        public ActionResult CrearMovimiento(long Id)//id de la cuenta
        {
            return View(new Models.Binding.Movimiento()
            {
                CuentaId = Id
            });
        }
        [HttpPost]
        public ActionResult CrearMovimiento(Models.Binding.Movimiento model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CuentaService cs = new CuentaService();
            if (cs.AgregarMovimiento(User.Identity.GetUserId<long>(), model))
                return RedirectToAction("Index", new { Id = model.CuentaId });
            else
            {
                ModelState.AddModelError("Error", "No se pudo agregar el movimiento, intentelo más tarde");
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult CambiarTea(long Id)//id de la cuenta
        {
            return View(new Models.Binding.TEA()
            {
                CuentaId = Id
            });
        }
        [HttpPost]
        public ActionResult CambiarTea(Models.Binding.TEA model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CuentaService cs = new CuentaService();
            if (cs.CambiarTEA(User.Identity.GetUserId<long>(), model))
                return RedirectToAction("Index", new { Id = model.CuentaId });
            else
            {
                ModelState.AddModelError("Error", "No se pudo cambiar la TEA, intentelo más tarde");
                return View(model);
            }
        }
        [HttpPost]
        public ActionResult EliminarSimulacion(long cuentaId)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            CuentaService cs = new CuentaService();
            cs.EliminarCuenta(User.Identity.GetUserId<long>(), cuentaId);
            return RedirectToAction("Index");
        }
        public JsonResult CalcularHastaFecha(long cuentaId, DateTime? fecha)
        {
            if (!ModelState.IsValid || !fecha.HasValue)
            {
                if (!fecha.HasValue)
                    ModelState.AddModelError("Fecha", "Parámetro no ingresado");
                List<string> list = new List<string>();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        list.Add(error.ErrorMessage);
                    }
                }
                return Json(new Helpers.JSONResponse()
                {
                    status = "Fail",
                    errors = list
                });
            }
            CuentaService cs = new CuentaService();
            double ans = cs.RecalcularHastaFecha(User.Identity.GetUserId<long>(), cuentaId, fecha.Value);
            if (ans == -1)
            {
                List<string> list = new List<string>();
                list.Add("Datos No Válidos");
                return Json(new Helpers.JSONResponse()
                {
                    status = "Fail",
                    errors = list
                });
            }
            else
            {
                return Json(new Helpers.JSONResponse()
                {
                    status = "Ok",
                    information = ans.ToString()
                });
            }
        }
    }
}