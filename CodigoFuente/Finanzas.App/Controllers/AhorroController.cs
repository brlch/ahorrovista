﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finanzas.Models.View;
using Finanzas.App.Services;
using Microsoft.AspNet.Identity;

namespace Finanzas.App.Controllers
{
    [Authorize]
    public class AhorroController : Controller
    {
        // GET: Ahorro
        [HttpGet]
        public ActionResult Index(long? id)
        {
            CuentaService cs = new CuentaService();
            Models.View.Cuenta model = null;
            if (id.HasValue)
                model = cs.GetCuenta(User.Identity.GetUserId<long>(), id.Value);
            if (model == null)
                return View(cs.GetCuentas(User.Identity.GetUserId<long>()));
            else
                return View("IndexCuenta", model);
        }


        [HttpGet]
        public ActionResult CrearCuenta()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CrearCuenta(Models.Binding.Cuenta cuenta)
        {
            if (!ModelState.IsValid)
                return View(cuenta);
            CuentaService cs = new CuentaService();
            long Id = cs.AgregarCuenta(User.Identity.GetUserId<long>(), cuenta);
            return RedirectToAction("Index", new { id = Id });
            //redirect to Cuenta-> id cuenta
        }
        [HttpGet]
        public ActionResult CrearMovimiento(long Id)//id de la cuenta
        {
            return View(new Models.Binding.Movimiento()
            {
                CuentaId = Id
            });
        }
        [HttpPost]
        public ActionResult CrearMovimiento(Models.Binding.Movimiento model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CuentaService cs = new CuentaService();
            if (cs.AgregarMovimientoFinal(User.Identity.GetUserId<long>(), model))
                return RedirectToAction("Index", new { Id = model.CuentaId });
            else
            {
                ModelState.AddModelError("Error", "No se pudo agregar el movimiento, intentelo más tarde");
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult CambiarTea(long Id)//id de la cuenta
        {
            return View(new Models.Binding.TEA()
            {
                CuentaId = Id
            });
        }
        [HttpPost]
        public ActionResult CambiarTea(Models.Binding.TEA model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CuentaService cs = new CuentaService();
            if (cs.CambiarTEAFinal(User.Identity.GetUserId<long>(), model))
                return RedirectToAction("Index", new { Id = model.CuentaId });
            else
            {
                ModelState.AddModelError("Error", "No se pudo cambiar la TEA, intentelo más tarde");
                return View(model);
            }
        }
        [HttpPost]
        public ActionResult EliminarCuenta(long cuentaId)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            CuentaService cs = new CuentaService();
            cs.EliminarCuenta(User.Identity.GetUserId<long>(), cuentaId);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult FinalizarCuenta(long cuentaId, DateTime fecha)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", new { Id = cuentaId });
            }
            CuentaService cs = new CuentaService();
            if (cs.FinalizarCuenta(User.Identity.GetUserId<long>(), cuentaId, fecha))
                return RedirectToAction("Index");
            else
                return RedirectToAction("Index", new { Id = cuentaId });
        }

    }
}