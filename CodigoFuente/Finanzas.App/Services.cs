﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Finanzas.Identity.Models;
using Finanzas.Models;
using Finanzas.Models.Binding;
using Finanzas.Models.View;
using Finanzas.Helpers;

namespace Finanzas.App.Services
{
    public class CuentaService
    {

        public FinanzasEntities db { get; set; }
        public CuentaService(FinanzasEntities db = null)
        {
            this.db = (db != null) ? db : (new FinanzasEntities());
        }
        ~CuentaService()
        {
            db.Dispose();
        }

        #region Cuenta
        public long AgregarCuenta(long usuarioId, Models.Binding.Cuenta cuentaBind, bool simulacion = false)
        {
            cuentaBind.TEA = cuentaBind.TEA / 100;
            var cuenta = new Models.Cuentas()
            {
                Monto = cuentaBind.Monto.toDecimal(),
                Descripcion = cuentaBind.Descripcion,
                FechaInicio = cuentaBind.FechaInicio,
                Estado = true,
                Simulacion = simulacion,
                UsuarioId = usuarioId,
            };
            db.Cuentas.Add(cuenta);
            db.SaveChanges();
            var movimiento = new Models.Movimientos()
            {
                TEA = cuentaBind.TEA.toDecimal(),
                Monto = cuentaBind.Monto.toDecimal(),
                InteresDesc = 0,
                MontoDesc = 0,
                CuentaId = cuenta.Id,
                Fecha = cuentaBind.FechaInicio,
                Descripcion = cuentaBind.Descripcion,
            };
            db.Movimientos.Add(movimiento);
            db.SaveChanges();
            return cuenta.Id;
        }
        public bool EliminarCuenta(long usuarioId, long cuentaId)
        {
            var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == cuentaId && x.UsuarioId == usuarioId && x.Estado == true);
            if (cuenta == null)//Si no existe la cuenta
                return false;
            cuenta.Estado = false;
            db.SaveChanges();

            return true;
        }


        public bool AgregarMovimientoFinal(long usuarioId, Models.Binding.Movimiento movBind)
        {
            if (movBind.Monto <= 0 || movBind.MonDescuento <= 0)
                return false;
            if (movBind.Retiro)
                movBind.Monto *= -1;
            movBind.MonDescuento *= -1;
            var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == movBind.CuentaId && x.UsuarioId == usuarioId && x.Estado == true && !x.FechaFin.HasValue);
            if (cuenta == null)//Si no existe la cuenta
                return false;
            if (cuenta.FechaFin != null)//Si ya terminó la cuenta
                return false;

            var ultimoMovimiento = db.Movimientos.Where(x => x.CuentaId == cuenta.Id).OrderByDescending(x => x.Fecha).FirstOrDefault();
            if (ultimoMovimiento == null)//Si no hay ultimo movimiento
                return false;

            if ((movBind.Fecha - ultimoMovimiento.Fecha).Days < 0)//Si la fecha ingresada es anterior a la ultima
                return false;

            var resultadoMovimiento = VAN(cuenta.Monto.toDouble(), ultimoMovimiento.TEA.toDouble(),
                (movBind.Fecha - ultimoMovimiento.Fecha).Days) + movBind.Monto + movBind.MonDescuento;
            if (resultadoMovimiento < 0)//Si retira más de lo que hay
                return false;

            movBind.Fecha = movBind.Fecha.Add(new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
            //Crear movimiento
            Models.Movimientos movimiento = new Models.Movimientos()
            {
                CuentaId = movBind.CuentaId,
                Monto = movBind.Monto.toDecimal(),
                Fecha = movBind.Fecha,
                Descripcion = movBind.Descripcion,
                InteresDesc = movBind.IntDescuento.toDecimal(),
                MontoDesc = movBind.MonDescuento.toDecimal(),
                TEA = ultimoMovimiento.TEA
            };
            cuenta.Monto = resultadoMovimiento.toDecimal();
            db.Movimientos.Add(movimiento);

            db.SaveChanges();

            return true;
        }
        public bool AgregarMovimiento(long usuarioId, Models.Binding.Movimiento movBind)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                if (movBind.Monto <= 0 || movBind.MonDescuento <= 0)
                    return false;
                if (movBind.Retiro)
                    movBind.Monto *= -1;
                movBind.MonDescuento *= -1;
                //var usuario = db.Usuarios.FirstOrDefault(x => x.Id == usuarioId);
                var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == movBind.CuentaId && x.UsuarioId == usuarioId && x.Estado && !x.FechaFin.HasValue);
                if (cuenta == null)//Si no existe la cuenta
                    return false;
                if (cuenta.FechaFin != null)//Si ya terminó la cuenta
                    return false;
                var ultimoMovimiento = db.Movimientos.Where(x => x.CuentaId == cuenta.Id).OrderByDescending(x => x.Fecha).FirstOrDefault();
                if (ultimoMovimiento == null)//Si no hay ultimo movimiento
                    return false;
                if ((movBind.Fecha - cuenta.FechaInicio).Days < 0)//Si la fecha ingresada es anterior a la creacion de la cuenta
                    return false;


                Models.Movimientos movimiento = new Models.Movimientos()
                {
                    CuentaId = movBind.CuentaId,
                    Monto = movBind.Monto.toDecimal(),
                    Fecha = movBind.Fecha,
                    Descripcion = movBind.Descripcion,
                    MontoDesc = movBind.MonDescuento.toDecimal(),
                };
                db.Movimientos.Add(movimiento);

                db.SaveChanges();
                double resultadoCuenta;
                if (!RecalcularTodo(usuarioId, cuenta, out resultadoCuenta))
                {
                    transaction.Rollback();
                    return false;
                }

                cuenta.Monto = resultadoCuenta.toDecimal();

                db.SaveChanges();
                transaction.Commit();
                return true;
            }
        }
        public bool CambiarTEAFinal(long usuarioId, Models.Binding.TEA teaBind)
        {
            teaBind.tea = teaBind.tea / 100;
            var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == teaBind.CuentaId && x.UsuarioId == usuarioId && x.Estado == true && !x.FechaFin.HasValue);
            if (cuenta == null)//Si no existe la cuenta
                return false;
            if (cuenta.FechaFin != null)//Si ya terminó la cuenta
                return false;

            var ultimoMovimiento = db.Movimientos.Where(x => x.CuentaId == cuenta.Id).OrderByDescending(x => x.Fecha).FirstOrDefault();
            if (ultimoMovimiento == null)//Si no hay ultimo movimiento
                return false;

            if ((teaBind.Fecha - ultimoMovimiento.Fecha).Days < 0)//Si la fecha ingresada es anterior a la ultima
                return false;

            var resultadoMovimiento = VAN(cuenta.Monto.toDouble(), ultimoMovimiento.TEA.toDouble(),
               (teaBind.Fecha - ultimoMovimiento.Fecha).Days);
            cuenta.Monto = resultadoMovimiento.toDecimal();

            Models.Movimientos movimiento = new Models.Movimientos()
            {
                CuentaId = cuenta.Id,
                Fecha = teaBind.Fecha,
                Monto = 0,
                Descripcion = teaBind.Descripcion,
                TEA = teaBind.tea.toDecimal(),
                InteresDesc = 0,
                MontoDesc = 0
            };

            db.Movimientos.Add(movimiento);
            db.SaveChanges();
            return true;

        }
        public bool CambiarTEA(long usuarioId, Models.Binding.TEA teaBind)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                teaBind.tea = teaBind.tea / 100;
                var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == teaBind.CuentaId && x.UsuarioId == usuarioId && x.Estado == true && !x.FechaFin.HasValue);
                if (cuenta == null)//Si no existe la cuenta
                    return false;
                if (cuenta.FechaFin != null)//Si ya terminó la cuenta
                    return false;

                var ultimoMovimiento = db.Movimientos.Where(x => x.CuentaId == cuenta.Id).OrderByDescending(x => x.Fecha).FirstOrDefault();
                if (ultimoMovimiento == null)//Si no hay ultimo movimiento
                    return false;

                if ((teaBind.Fecha - cuenta.FechaInicio).Days < 0)//Si la fecha ingresada es anterior a la ultima
                    return false;


                Models.Movimientos movimiento = new Models.Movimientos()
                {
                    CuentaId = cuenta.Id,
                    Fecha = teaBind.Fecha,
                    Monto = 0,
                    Descripcion = teaBind.Descripcion,
                    TEA = teaBind.tea.toDecimal(),
                    InteresDesc = 0,
                    MontoDesc = 0
                };
                db.Movimientos.Add(movimiento);
                db.SaveChanges();
                double resultadoCuenta;
                if (!RecalcularTodo(usuarioId, cuenta, out resultadoCuenta))
                {
                    transaction.Rollback();
                    return false;
                }

                cuenta.Monto = resultadoCuenta.toDecimal();
                db.SaveChanges();
                transaction.Commit();
                return true;
            }

        }
        public bool RecalcularTodo(long usuarioId, Models.Cuentas cuenta, out double monto)
        {
            monto = 0;

            var movimientos = db.Movimientos.Where(x => x.CuentaId == cuenta.Id).OrderBy(x => x.Fecha).ToList();
            double tea = movimientos[0].TEA.toDouble();
            for (int i = 0; i < movimientos.Count; i++)
            {
                monto += movimientos[i].Monto.toDouble();
                monto += movimientos[i].MontoDesc.toDouble();
                if (monto < 0)
                    return false;
                if (movimientos[i].Monto == 0)
                    tea = movimientos[i].TEA.toDouble();
                if (i < movimientos.Count - 1)
                    monto = VAN(monto, tea,
                        (movimientos[i + 1].Fecha - movimientos[i].Fecha).Days);
            }
            return true;
        }
        public bool FinalizarCuenta(long usuarioId, long cuentaId, DateTime fecha)
        {
            var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == cuentaId && x.UsuarioId == usuarioId && x.Estado == true && !x.FechaFin.HasValue);
            if (cuenta == null)//Si no existe la cuenta
                return false;
            if (cuenta.FechaFin != null)//Si ya terminó la cuenta
                return false;

            var ultimoMovimiento = db.Movimientos.Where(x => x.CuentaId == cuenta.Id).OrderByDescending(x => x.Fecha).FirstOrDefault();
            if (ultimoMovimiento == null)//Si no hay ultimo movimiento
                return false;

            if ((fecha - ultimoMovimiento.Fecha).Days < 0)//Si la fecha ingresada es anterior a la ultima
                return false;
            var resultadoMovimiento = VAN(cuenta.Monto.toDouble(), ultimoMovimiento.TEA.toDouble(),
               (fecha - ultimoMovimiento.Fecha).Days);
            cuenta.Monto = resultadoMovimiento.toDecimal();
            cuenta.FechaFin = fecha;
            db.SaveChanges();

            return true;
        }

        #endregion

        #region ObtenerDatos
        public Models.View.Cuentas GetCuentas(long usuarioId, bool esSimulacion = false)
        {
            var tempCuentas = db.Cuentas.Where(x => x.UsuarioId == usuarioId && x.Estado && x.Simulacion == esSimulacion).ToList();

            List<Models.View.Cuenta> lista = new List<Models.View.Cuenta>();

            if (tempCuentas.Count == 0)
            {
                Models.View.Cuentas cuentasVacia = new Models.View.Cuentas()
                {
                    Ahorros = lista
                };
                return cuentasVacia;
            }

            foreach (var item in tempCuentas)
            {
                lista.Add(item.toCuenta());
            }
            Models.View.Cuentas cuentas = new Models.View.Cuentas()
            {
                Ahorros = lista
            };

            return cuentas;
        }
        public Models.View.Cuenta GetCuenta(long usuarioId, long cuentaId, bool conMovimientos = true, bool esSimulacion = false)
        {
            var usuario = db.Usuarios.FirstOrDefault(x => x.Id == usuarioId);
            if (usuario == null)
                return null;

            var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == cuentaId && x.UsuarioId == usuarioId && x.Simulacion == esSimulacion && x.Estado);
            if (cuenta == null)
                return null;

            Models.View.Cuenta cuentaRet;

            if (conMovimientos)
            {
                var movimientos = db.Movimientos.Where(x => x.CuentaId == cuentaId).OrderByDescending(x => x.Fecha).ToList();
                if (movimientos == null)
                    return null;
                cuentaRet = cuenta.toCuenta(movimientos);
                var ultimomov = movimientos[0];
                if (ultimomov.Fecha < DateTime.Now)
                {
                    cuentaRet.MontoActual = VAN(cuentaRet.Monto, ultimomov.TEA.toDouble(), (DateTime.Now - ultimomov.Fecha).Days);
                }
                else
                    cuentaRet.MontoActual = cuentaRet.Monto;
            }
            else
            {
                cuentaRet = cuenta.toCuenta();
            }
            //cuentaRet.movimientoBinding.CuentaId = cuentaId;
            return cuentaRet;
        }
        public double RecalcularHastaFecha(long usuarioId, long cuentaId, DateTime fechaLimite)
        {
            var cuenta = db.Cuentas.FirstOrDefault(x => x.Id == cuentaId && x.UsuarioId == usuarioId);
            if (cuenta == null)
                return -1;
            double monto = 0;
            fechaLimite = fechaLimite.AddHours(23.98);
            var movimientos = db.Movimientos.Where(x => x.CuentaId == cuentaId && x.Fecha <= fechaLimite).OrderBy(x => x.Fecha).ToList();
            if (movimientos == null)
                return -1;
            if (movimientos.Count == 0)
                return -1;
            double tea = movimientos[0].TEA.toDouble();
            for (int i = 0; i < movimientos.Count; i++)
            {
                monto += movimientos[i].Monto.toDouble();
                monto += movimientos[i].MontoDesc.toDouble();
                if (movimientos[i].Monto == 0)
                    tea = movimientos[i].TEA.toDouble();
                if (i < movimientos.Count - 1)
                    monto = VAN(monto, tea, (movimientos[i + 1].Fecha - movimientos[i].Fecha).Days);
                else
                    monto = VAN(monto, tea, (fechaLimite - movimientos[i].Fecha).Days);
            }

            return monto;
        }
        #endregion

        #region Helpers
        public static double VAN(double monto, double TEA, int dias)
        {
            //formula de llevar al futuro
            double TEM = Math.Pow(TEA + 1, dias * 1.0 / 360);
            return monto * TEM;
        }
        #endregion
    }
}
